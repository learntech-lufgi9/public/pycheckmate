import unittest
import textwrap
from pycheckmate import PyCheckMate


class TestLambdaUsed(unittest.TestCase):

    def test_lambda_not_used(self):
        source_code = textwrap.dedent("""
            a = 2
            b = 3
            c = a + b
        """)
        pcm = PyCheckMate(source_code)

        self.assertFalse(pcm.lambda_used()['passed'])

    def test_lambda_used(self):
        source_code = [
        textwrap.dedent("""
            lambda x: x * 2
        """),
        textwrap.dedent("""
            test_lambda = lambda x: x * 2
        """),
        textwrap.dedent("""
            def test():
                lambda x: x * 2
        """),
        textwrap.dedent("""
            def test():
                return lambda x: x * 2
        """)
        ]

        for elem in source_code:
            pcm = PyCheckMate(elem)

            self.assertTrue(pcm.lambda_used()['passed'])

    def test_lambda_used_name(self):
        source_code = textwrap.dedent("""
            test_lambda = lambda x: x * 2
        """)

        pcm = PyCheckMate(source_code)

        self.assertTrue(pcm.lambda_used("test_lambda")['passed'])
        self.assertFalse(pcm.lambda_used("test_lambda_not_existing")['passed'])

    def test_lambda_used_params(self):
        source_code = textwrap.dedent("""
            test_lambda = lambda x, y: x * y * 2
        """)

        pcm = PyCheckMate(source_code)

        self.assertTrue(pcm.lambda_used("test_lambda", 2)['passed'])
        self.assertFalse(pcm.lambda_used("test_lambda", 1)['passed'])
        self.assertFalse(pcm.lambda_used("test_lambda_not_existing", 2)['passed'])


class TestFunctionUsesLambda(unittest.TestCase):

    def test_function_uses_not_lambda(self):
        source_code = [
        textwrap.dedent("""
            def test():
                a = 2
                b = 3
                c = a + b
        """),
        textwrap.dedent("""
            def test():
                a = 2
                b = 3
                c = a + b
                
            lambda x: x * 2
        """)
        ]

        for elem in source_code:
            pcm = PyCheckMate(elem)

            self.assertFalse(pcm.function_uses_lambda("test")['passed'])

    def test_function_uses_lambda(self):
        source_code = textwrap.dedent("""
            def test():
                lambda x: x * 2
        """)
        pcm = PyCheckMate(source_code)

        self.assertTrue(pcm.function_uses_lambda("test")['passed'])