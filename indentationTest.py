import ast
import textwrap


def remove_leading_whitespace(text):
    """Help function that removes leading whitespaces for all non-comment, non-docstring and not empty lines."""
    lines = text.splitlines()

    # Get the number of leading whitespaces for the first non-comment, non-docstring and not empty line
    first_line_whitespace_count = None
    for line in lines:
        if not line.strip() or line.strip().startswith(('#', "'''", '"""')):
            continue
        first_line_whitespace_count = len(line) - len(line.lstrip())
        break

    # If there is no non-comment, non-docstring line, return True (as there is nothing to compare)
    if first_line_whitespace_count is None:
        return text

    print("first_line_whitespace_count: ", first_line_whitespace_count)

    cleaned_lines = []
    inside_docstring = False

    for line in lines:
        # Check if the line starts a docstring
        if line.strip().startswith(('\'\'\'', '"""')):
            inside_docstring = not inside_docstring

        # If inside a docstring or comment, keep the line unchanged
        if inside_docstring or line.strip().startswith(('#', "'''", '"""')):
            cleaned_lines.append(line)
        else:
            # Remove leading whitespaces from non-comment, non-docstring lines
            #cleaned_lines.append(line.lstrip())
            if len(line) - len(line.lstrip()) >= first_line_whitespace_count:#line.startswith(" "):
                cleaned_lines.append(line[first_line_whitespace_count:])
            else:
                #raise SyntaxError
                return text

    cleaned_text = "\n".join(cleaned_lines)
    return cleaned_text


source = """
temperatures_celsius = [8.4, 15.0, 12.5, 8.4, -1.9, 15.0, 2.7]
#del temperatures_celsius[3]
temperatures_celsius.pop(3)

def test():
    a = 1
    b = 2
 """


source_2 = """
 temperatures_celsius = [8.4, 15.0, 12.5, 8.4, -1.9, 15.0, 2.7]
#del temperatures_celsius[3]
temperatures_celsius.pop(3)

 def test():
    a = 1
    b = 2
 """


try:
    print(textwrap.dedent(source_2))
    #ast.parse(source)
    #print(remove_leading_whitespace(source_2))
    #ast.parse(source_2)
    #ast.parse(remove_leading_whitespace(source_2))
    #ast.parse(source_3)
    #ast.parse(source_4)
except SyntaxError as e:
    error_line_number = e.lineno - 1
    error_type = e.__class__.__name__
    print(f"Code contains {error_type} at line {error_line_number}.")
