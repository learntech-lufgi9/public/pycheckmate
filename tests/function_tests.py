import unittest
import textwrap
from pycheckmate import PyCheckMate


class TestHasFunction(unittest.TestCase):

    def test_has_not_function(self):
        source_code = textwrap.dedent("""
            def not_existing_test():
                a = 2
                b = 3
                c = a + b
        """)
        pcm = PyCheckMate(source_code)

        self.assertFalse(pcm.has_function("test")['passed'])
        self.assertFalse(pcm.has_function("test", 0)['passed'])
        self.assertFalse(pcm.has_function("test", 1)['passed'])

    def test_has_function(self):
        source_code = textwrap.dedent("""
            def test():
                a = 2
                b = 3
                c = a + b
        """)
        pcm = PyCheckMate(source_code)

        self.assertTrue(pcm.has_function("test")['passed'])
        self.assertTrue(pcm.has_function("test", 0)['passed'])
        self.assertFalse(pcm.has_function("test", 1)['passed'])

    def test_has_function_params(self):
        source_code = [
        textwrap.dedent("""
            def test(param1, param2, param3):
                a = 2
                b = 3
                c = a + b
        """),
        textwrap.dedent("""
            def test(param1: int, param2 = 0, param3: str = "empty"):
                a = 2
                b = 3
                c = a + b
        """),
        ]

        for elem in source_code:
            pcm = PyCheckMate(elem)

            self.assertTrue(pcm.has_function("test")['passed'])
            self.assertFalse(pcm.has_function("test", 0)['passed'])
            self.assertTrue(pcm.has_function("test", 3)['passed'])

class TestHasFunctions(unittest.TestCase):

    def test_has_functions(self):
        source_code = textwrap.dedent("""
            def test_1():
                pass
                
            def test_2(param1):
                pass
                
            def test_3(param1, param2):
                pass
        """)
        pcm = PyCheckMate(source_code)

        self.assertTrue(pcm.has_functions({"test_1"})['passed'])
        self.assertTrue(pcm.has_functions({"test_1", "test_2", "test_3"})['passed'])

        self.assertFalse(pcm.has_functions({"test_4"})['passed'])
        self.assertFalse(pcm.has_functions({"test_1", "test_2", "test_3", "test_4"})['passed'])

class TestFunctionHasParameters(unittest.TestCase):

    def test_function_has_not_parameters(self):
        source_code = textwrap.dedent("""
d           def test():
                pass
        """)

        pcm = PyCheckMate(source_code)

        self.assertFalse(pcm.function_has_parameters("test_not_existing", {"param1": {}, "param2": {}, "param3": {}})['passed'])
        self.assertFalse(pcm.function_has_parameters("test", {"param1": {}, "param2": {}, "param3": {}})['passed'])

        self.assertFalse(pcm.function_has_parameters("test", {}, True)['passed'])
        self.assertFalse(pcm.function_has_parameters("test", {}, False, True)['passed'])
        self.assertFalse(pcm.function_has_parameters("test", {}, True, True)['passed'])

        self.assertFalse(pcm.function_has_parameters("test", {}, "args")['passed'])
        self.assertFalse(pcm.function_has_parameters("test", {}, False, "kwargs")['passed'])
        self.assertFalse(pcm.function_has_parameters("test", {}, "args", "kwargs")['passed'])

    def test_function_has_parameters_args(self):
        source_code = textwrap.dedent("""
            def test(param1: int, param2 = 0, param3: bool = True):
                pass
        """)

        pcm = PyCheckMate(source_code)

        self.assertTrue(pcm.function_has_parameters("test", {"param1": {}, "param2": {}, "param3": {}})['passed'])
        self.assertTrue(pcm.function_has_parameters("test", {"param1": {"type": int}, "param2": {'default': 0}, "param3": {"type": bool, 'default': True}})['passed'])

        self.assertFalse(pcm.function_has_parameters("test", {"param1": {"type": str}, "param2": {'default': 20}, "param3": {"type": int, 'default': 0}})['passed'])

        self.assertFalse(pcm.function_has_parameters("test", {}, True)['passed'])
        self.assertFalse(pcm.function_has_parameters("test", {}, False, True)['passed'])
        self.assertFalse(pcm.function_has_parameters("test", {}, True, True)['passed'])
        self.assertFalse(pcm.function_has_parameters("test", {}, "args")['passed'])
        self.assertFalse(pcm.function_has_parameters("test", {}, False, "kwargs")['passed'])
        self.assertFalse(pcm.function_has_parameters("test", {}, "args", "kwargs")['passed'])

    def test_function_has_parameters_args_via_typing_module(self):
        source_code = textwrap.dedent("""
            city_names = ['Aachen', 'Eschweiler', 'Wuerselen', 'Alsdorf', 'Stolberg']
            city_postal_codes = [52062, 52249, 52146, 52477, 52222]
            
            from typing import List
            
            def create_city_dic(names: List[str], postal_codes: List[int]) -> dict:
                # Verwende zip() um die beiden Listen zu kombinieren und ein Wörterbuch zu erstellen
                city_dict = dict(zip(names, postal_codes))
            
                return city_dict
        """)

        pcm = PyCheckMate(source_code)

        self.assertTrue(pcm.function_has_parameters("create_city_dic", {"names": {}, "postal_codes": {}})['passed'])
        #T.B.D.
        #self.assertTrue(pcm.function_has_parameters("create_city_dic", {"names": {"type": int}, "postal_codes": {'type': }})['passed'])

    def test_function_has_parameters_vararg(self):
        source_code = textwrap.dedent("""
            def test(*args):
                pass
        """)

        pcm = PyCheckMate(source_code)

        self.assertTrue(pcm.function_has_parameters("test", {}, True)['passed'])
        self.assertTrue(pcm.function_has_parameters("test", {}, "args")['passed'])

        self.assertFalse(pcm.function_has_parameters("test", {}, "test_args")['passed'])
        self.assertFalse(pcm.function_has_parameters("test", {}, True, True)['passed'])
        self.assertFalse(pcm.function_has_parameters("test", {}, "args", True)['passed'])

    def test_function_has_parameters_kwarg(self):
        source_code = textwrap.dedent("""
            def test(**kwarg):
                pass
        """)

        pcm = PyCheckMate(source_code)

        self.assertTrue(pcm.function_has_parameters("test", {}, False, True)['passed'])
        self.assertTrue(pcm.function_has_parameters("test", {}, False, "kwarg")['passed'])

        self.assertFalse(pcm.function_has_parameters("test", {}, False, "test_kwarg")['passed'])
        self.assertFalse(pcm.function_has_parameters("test", {}, True, True)['passed'])
        self.assertFalse(pcm.function_has_parameters("test", {}, True, "kwarg")['passed'])

class TestFunctionUsesFunctions(unittest.TestCase):

    def test_function_uses_functions(self):
        source_code = textwrap.dedent("""
            def test():
                lst = [1,2,3,4]
                len(lst)
                lst.append(1)
        """)

        pcm = PyCheckMate(source_code)

        self.assertFalse(pcm.function_uses_functions('test_not_existing', ['append', 'len'])['passed'])

        self.assertTrue(pcm.function_uses_functions('test', ['append', 'len'])['passed'])
        self.assertTrue(pcm.function_uses_functions('test', ['append'])['passed'])
        self.assertTrue(pcm.function_uses_functions('test', ['len'])['passed'])

        self.assertFalse(pcm.function_uses_functions('test', ['append', 'len', 'min'])['passed'])

    def test_function_uses_function_from_module_import_all(self):
        source_code = textwrap.dedent("""
            from random import *
            
            def test(size, min_value, max_value):
                random = []
                for i in range(size):
                    random.append(randint(min_value, max_value))
                return random 

        """)

        pcm = PyCheckMate(source_code)

        self.assertFalse(pcm.function_uses_functions('test_not_existing', ['randint'])['passed'])

        self.assertTrue(pcm.function_uses_functions('test', ['randint'])['passed'])



class TestFunctionUsesModuleFunctions(unittest.TestCase):

    def test_function_not_uses_module_function(self):
        source_code = [
            textwrap.dedent("""
                a = 2
            """),
            textwrap.dedent("""
                import numpy
                
                numcheck = numpy.arange(15, dtype=np.int64)
                
                def test():
                    a = 2
            """),
            textwrap.dedent("""
                import numpy

                def test():
                    numcheck = numpy.arange(15, dtype=np.int64)
            """),
        ]

        for elem in source_code:
            pcm = PyCheckMate(elem)

            self.assertFalse(pcm.function_uses_module_function('test', ["reshape"], "numpy")['passed'])

    def test_function_uses_module_function_one_function(self):
        source_code = [
            textwrap.dedent("""
                import numpy as np
                
                def test():
                    numcheck = np.arange(15, dtype=np.int64).reshape(3, 5)
            """),
            textwrap.dedent("""
                import numpy
                
                def test():
                    numcheck = numpy.arange(15, dtype=np.int64).reshape(3, 5)
            """),
            # this code snippet is currenlty not working
            textwrap.dedent("""
                from numpy import arange
                
                def test():
                    numcheck = arange(15, dtype=np.int64)
            """),
            textwrap.dedent("""
                from numpy import *
                
                def test():
                    numcheck = arange(15, dtype=np.int64)
            """)
        ]

        for elem in source_code:
            pcm = PyCheckMate(elem)

            self.assertTrue(pcm.function_uses_module_function('test', ["arange"], "numpy")['passed'])

    def test_function_uses_module_function_multiple_function(self):
        source_code = [
            textwrap.dedent("""
                import numpy as np
                def test():
                    numcheck = np.arange(15, dtype=np.int64).reshape(3, 5)
            """),
            textwrap.dedent("""
                import numpy
                def test():
                    numcheck = numpy.arange(15, dtype=np.int64).reshape(3, 5)
            """),
            # these code snippet are currenlty not working
            #textwrap.dedent("""
            #    from numpy import arange
            #    from numpy import reshape
            #    def test():
            #        numcheck = arange(15, dtype=np.int64).reshape(3, 5)
            #"""),
            #textwrap.dedent("""
            #    import numpy
            #    def test():
            #        numcheck = np.arange(15, dtype=np.int64)
            #        numcheck.reshape(3, 5)
            #"""),

        ]

        for elem in source_code:
            pcm = PyCheckMate(elem)

            self.assertTrue(pcm.function_uses_module_function('test', ["arange", "reshape"], "numpy")['passed'])

class TestBuiltInFunctionUsed(unittest.TestCase):

    def test_built_in_function_not_used(self):
        source_code = [
            textwrap.dedent("""
                lst = [1,2,3]
                lst.append(4)
            """),
            textwrap.dedent("""
                lst = [1,2,3]
                print(min(lst))
            """)
        ]

        for elem in source_code:
            pcm = PyCheckMate(elem)

            self.assertFalse(pcm.built_in_function_used('len')['passed'])


    def test_built_in_function_used_function(self):
        source_code = [
            textwrap.dedent("""
                lst = [1,2,3]
                len(lst)
            """),
            textwrap.dedent("""
                lst = [1,2,3]
                length= len(lst)
            """),
            textwrap.dedent("""
                def test():
                    lst = [1,2,3]
                    len(lst)
            """)
        ]

        for elem in source_code:
            pcm = PyCheckMate(elem)

            self.assertTrue(pcm.built_in_function_used('len')['passed'])

    def test_built_in_function_used_method(self):
        source_code = [
            textwrap.dedent("""
                lst = [1,2,3]
                lst.append(4)
            """),
            textwrap.dedent("""
                lst = [1,2,3]
                new_lst= lst.append(4)
            """),
            textwrap.dedent("""
                def test():
                    lst = [1,2,3]
                    lst.append(4)
            """)
        ]

        for elem in source_code:
            pcm = PyCheckMate(elem)

            self.assertTrue(pcm.built_in_function_used('append')['passed'])