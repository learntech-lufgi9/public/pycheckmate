import unittest
import textwrap
from pycheckmate import PyCheckMate


class TestForUsed(unittest.TestCase):

    def test_for_not_used(self):
        source_code = textwrap.dedent("""
            a = 2
            b = 3
            c = a + b
        """)
        pcm = PyCheckMate(source_code)

        self.assertFalse(pcm.for_used()['passed'])
        self.assertFalse(pcm.for_used(comprehension=True)['passed'])

        self.assertFalse(pcm.for_used({"range": (10,)})['passed'])
        self.assertFalse(pcm.for_used({"range": (0, 10)})['passed'])
        self.assertFalse(pcm.for_used({"range": (0, 10, 1)})['passed'])
        self.assertFalse(pcm.for_used({"range": (10,)}, comprehension=True)['passed'])
        self.assertFalse(pcm.for_used({"range": (0, 10)}, comprehension=True)['passed'])
        self.assertFalse(pcm.for_used({"range": (0, 10, 1)}, comprehension=True)['passed'])


        self.assertFalse(pcm.for_used({"variable": "abc"})['passed'])
        self.assertFalse(pcm.for_used({"variable": "abc"}, comprehension=True)['passed'])

    def test_for_used_range_stop(self):
        source_code = [
        textwrap.dedent("""
            for x in range(10):
                print(x)
        """),
        textwrap.dedent("""
            def test():
                for x in range(10):
                    print(x)
        """)
        ]

        for elem in source_code:
            pcm = PyCheckMate(elem)

            self.assertTrue(pcm.for_used()['passed'])
            self.assertTrue(pcm.for_used(comprehension=True)['passed'])

            self.assertTrue(pcm.for_used({"range": (10,)})['passed'])
            self.assertTrue(pcm.for_used({"range": (0, 10)})['passed'])
            self.assertTrue(pcm.for_used({"range": (0, 10, 1)})['passed'])
            self.assertTrue(pcm.for_used({"range": (10,)}, comprehension=True)['passed'])
            self.assertTrue(pcm.for_used({"range": (0, 10)}, comprehension=True)['passed'])
            self.assertTrue(pcm.for_used({"range": (0, 10, 1)}, comprehension=True)['passed'])

            self.assertFalse(pcm.for_used({"range": (15,)})['passed'])
            self.assertFalse(pcm.for_used({"range": (1, 10)})['passed'])
            self.assertFalse(pcm.for_used({"range": (0, 10, 2)})['passed'])
            self.assertFalse(pcm.for_used({"range": (15,)}, comprehension=True)['passed'])
            self.assertFalse(pcm.for_used({"range": (1, 10)}, comprehension=True)['passed'])
            self.assertFalse(pcm.for_used({"range": (0, 10, 2)}, comprehension=True)['passed'])

            self.assertFalse(pcm.for_used({"variable": "abc"})['passed'])
            self.assertFalse(pcm.for_used({"variable": "abc"}, comprehension=True)['passed'])


    def test_for_used_range_start_stop(self):
        source_code = textwrap.dedent("""
            for x in range(0, 10):
                print(x)
        """)
        pcm = PyCheckMate(source_code)

        self.assertTrue(pcm.for_used()['passed'])
        self.assertTrue(pcm.for_used(comprehension=True)['passed'])

        self.assertTrue(pcm.for_used({"range": (10,)})['passed'])
        self.assertTrue(pcm.for_used({"range": (0, 10)})['passed'])
        self.assertTrue(pcm.for_used({"range": (0, 10, 1)})['passed'])
        self.assertTrue(pcm.for_used({"range": (10,)}, comprehension=True)['passed'])
        self.assertTrue(pcm.for_used({"range": (0, 10)}, comprehension=True)['passed'])
        self.assertTrue(pcm.for_used({"range": (0, 10, 1)}, comprehension=True)['passed'])

        self.assertFalse(pcm.for_used({"range": (15,)})['passed'])
        self.assertFalse(pcm.for_used({"range": (1, 10)})['passed'])
        self.assertFalse(pcm.for_used({"range": (0, 10, 2)})['passed'])
        self.assertFalse(pcm.for_used({"range": (15,)}, comprehension=True)['passed'])
        self.assertFalse(pcm.for_used({"range": (1, 10)}, comprehension=True)['passed'])
        self.assertFalse(pcm.for_used({"range": (0, 10, 2)}, comprehension=True)['passed'])

        self.assertFalse(pcm.for_used({"variable": "abc"})['passed'])
        self.assertFalse(pcm.for_used({"variable": "abc"}, comprehension=True)['passed'])

    def test_for_used_range_start_stop_step(self):
        source_code = textwrap.dedent("""
            for x in range(0, 10, 2):
                print(x)
        """)

        pcm = PyCheckMate(source_code)

        self.assertTrue(pcm.for_used()['passed'])
        self.assertTrue(pcm.for_used(comprehension=True)['passed'])


        self.assertTrue(pcm.for_used({"range": (0, 10, 2)})['passed'])
        self.assertTrue(pcm.for_used({"range": (0, 10, 2)}, comprehension=True)['passed'])

        self.assertFalse(pcm.for_used({"range": (10,)})['passed'])
        self.assertFalse(pcm.for_used({"range": (15,)})['passed'])
        self.assertFalse(pcm.for_used({"range": (0, 10)})['passed'])
        self.assertFalse(pcm.for_used({"range": (1, 10)})['passed'])
        self.assertFalse(pcm.for_used({"range": (0, 10, 1)})['passed'])
        self.assertFalse(pcm.for_used({"range": (10,)}, comprehension=True)['passed'])
        self.assertFalse(pcm.for_used({"range": (15,)}, comprehension=True)['passed'])
        self.assertFalse(pcm.for_used({"range": (0, 10)}, comprehension=True)['passed'])
        self.assertFalse(pcm.for_used({"range": (1, 10)}, comprehension=True)['passed'])
        self.assertFalse(pcm.for_used({"range": (0, 10, 1)}, comprehension=True)['passed'])

        self.assertFalse(pcm.for_used({"variable": "abc"})['passed'])
        self.assertFalse(pcm.for_used({"variable": "abc"}, comprehension=True)['passed'])

    def test_for_used_iteration(self):
        source_code = [
            textwrap.dedent("""
                test_object = [1, 2, 3, 4]
                for elem in test_object:
                    print(elem)
            """),
            textwrap.dedent("""
                test_object = [1, 2, 3, 4]
                for elem in test_object.reverse():
                    print(elem)
            """),
            textwrap.dedent("""
                test_object = {1: "1", 2: "2", 3: "3", 4: "4"}
                for elem in test_object:
                    print(elem)
            """),
            textwrap.dedent("""
                test_object = {1: "1", 2: "2", 3: "3", 4: "4"}
                for key, value in test_object.items():
                    print(key, value)
            """),
            textwrap.dedent("""
                test_object = {1: "1", 2: "2", 3: "3", 4: "4"}
                for elem in test_object.keys():
                    print(elem)
            """),
            textwrap.dedent("""
                test_object = {1: "1", 2: "2", 3: "3", 4: "4"}
                for elem in test_object.values():
                    print(elem)
            """),
        ]

        for elem in source_code:
            pcm = PyCheckMate(elem)

            self.assertTrue(pcm.for_used()['passed'])
            self.assertTrue(pcm.for_used(comprehension=True)['passed'])

            self.assertFalse(pcm.for_used({"range": (0, 10)})['passed'])
            self.assertFalse(pcm.for_used({"range": (0, 10, 2)})['passed'])
            self.assertFalse(pcm.for_used({"range": (10,)})['passed'])
            self.assertFalse(pcm.for_used({"range": (15,)})['passed'])
            self.assertFalse(pcm.for_used({"range": (1, 10)})['passed'])
            self.assertFalse(pcm.for_used({"range": (0, 10, 1)})['passed'])
            self.assertFalse(pcm.for_used({"range": (0, 10)}, comprehension=True)['passed'])
            self.assertFalse(pcm.for_used({"range": (0, 10, 2)}, comprehension=True)['passed'])
            self.assertFalse(pcm.for_used({"range": (10,)}, comprehension=True)['passed'])
            self.assertFalse(pcm.for_used({"range": (15,)}, comprehension=True)['passed'])
            self.assertFalse(pcm.for_used({"range": (1, 10)}, comprehension=True)['passed'])
            self.assertFalse(pcm.for_used({"range": (0, 10, 1)}, comprehension=True)['passed'])

            self.assertFalse(pcm.for_used({"variable": "abc"})['passed'])
            self.assertFalse(pcm.for_used({"variable": "abc"}, comprehension=True)['passed'])

            self.assertTrue(pcm.for_used({"variable": "test_object"})['passed'])
            self.assertTrue(pcm.for_used({"variable": "test_object"}, comprehension=True)['passed'])


    def test_for_used_comprehension_range_stop(self):
        source_code = [
            textwrap.dedent("""
                [x**2 for x in range(10)]
            """),
            textwrap.dedent("""
                quadrate = [x**2 for x in range(10)]
            """),
            textwrap.dedent("""
                {x: x**2 for x in range(10)}
            """),
            textwrap.dedent("""
                squared_dict = {x: x**2 for x in range(10)}
            """),
            textwrap.dedent("""
                {x**2 for x in range(10)}
            """),
            textwrap.dedent("""
                unique_squares = {x**2 for x in range(10)}
            """),
            textwrap.dedent("""
                [x**2 for x in range(10+1-1+2-2)]
            """),
        ]

        for elem in source_code:
            pcm = PyCheckMate(elem)

            self.assertFalse(pcm.for_used()['passed'])
            self.assertTrue(pcm.for_used(comprehension=True)['passed'])

            self.assertFalse(pcm.for_used({"range": (10,)})['passed'])
            self.assertFalse(pcm.for_used({"range": (0, 10)})['passed'])
            self.assertFalse(pcm.for_used({"range": (0, 10, 1)})['passed'])

            self.assertTrue(pcm.for_used({"range": (10,)}, comprehension=True)['passed'])
            self.assertTrue(pcm.for_used({"range": (0, 10)}, comprehension=True)['passed'])
            self.assertTrue(pcm.for_used({"range": (0, 10, 1)}, comprehension=True)['passed'])

            self.assertFalse(pcm.for_used({"range": (15,)})['passed'])
            self.assertFalse(pcm.for_used({"range": (1, 10)})['passed'])
            self.assertFalse(pcm.for_used({"range": (0, 10, 2)})['passed'])
            self.assertFalse(pcm.for_used({"range": (15,)}, comprehension=True)['passed'])
            self.assertFalse(pcm.for_used({"range": (1, 10)}, comprehension=True)['passed'])
            self.assertFalse(pcm.for_used({"range": (0, 10, 2)}, comprehension=True)['passed'])

            self.assertFalse(pcm.for_used({"variable": "abc"})['passed'])
            self.assertFalse(pcm.for_used({"variable": "abc"}, comprehension=True)['passed'])

    def test_for_used_comprehension_range_start_stop(self):
        source_code = [
            textwrap.dedent("""
                [x**2 for x in range(0, 10)]
            """),
            textwrap.dedent("""
                quadrate = [x**2 for x in range(0, 10)]
            """),
            textwrap.dedent("""
                {x: x**2 for x in range(0, 10)}
            """),
            textwrap.dedent("""
                squared_dict = {x: x**2 for x in range(0, 10)}
            """),
            textwrap.dedent("""
                {x**2 for x in range(0, 10)}
            """),
            textwrap.dedent("""
                unique_squares = {x**2 for x in range(0, 10)}
            """),
            textwrap.dedent("""
                [x**2 for x in range(0+1+1-2, 10+2-1-2+1)]
            """),
        ]

        for elem in source_code:
            pcm = PyCheckMate(elem)

            self.assertFalse(pcm.for_used()['passed'])
            self.assertTrue(pcm.for_used(comprehension=True)['passed'])

            self.assertFalse(pcm.for_used({"range": (10,)})['passed'])
            self.assertFalse(pcm.for_used({"range": (0, 10)})['passed'])
            self.assertFalse(pcm.for_used({"range": (0, 10, 1)})['passed'])
            self.assertTrue(pcm.for_used({"range": (10,)}, comprehension=True)['passed'])
            self.assertTrue(pcm.for_used({"range": (0, 10)}, comprehension=True)['passed'])
            self.assertTrue(pcm.for_used({"range": (0, 10, 1)}, comprehension=True)['passed'])

            self.assertFalse(pcm.for_used({"range": (15,)})['passed'])
            self.assertFalse(pcm.for_used({"range": (1, 10)})['passed'])
            self.assertFalse(pcm.for_used({"range": (0, 10, 2)})['passed'])
            self.assertFalse(pcm.for_used({"range": (15,)}, comprehension=True)['passed'])
            self.assertFalse(pcm.for_used({"range": (1, 10)}, comprehension=True)['passed'])
            self.assertFalse(pcm.for_used({"range": (0, 10, 2)}, comprehension=True)['passed'])

            self.assertFalse(pcm.for_used({"variable": "abc"})['passed'])
            self.assertFalse(pcm.for_used({"variable": "abc"}, comprehension=True)['passed'])



    def test_for_used_comprehension_range_start_stop_step(self):
        source_code = [
            textwrap.dedent("""
                [x**2 for x in range(0, 10, 2)]
            """),
            textwrap.dedent("""
                quadrate = [x**2 for x in range(0, 10, 2)]
            """),
            textwrap.dedent("""
                {x: x**2 for x in range(0, 10, 2)}
            """),
            textwrap.dedent("""
                squared_dict = {x: x**2 for x in range(0, 10, 2)}
            """),
            textwrap.dedent("""
                {x**2 for x in range(0, 10, 2)}
            """),
            textwrap.dedent("""
                unique_squares = {x**2 for x in range(0, 10, 2)}
            """),
        ]

        for elem in source_code:
            pcm = PyCheckMate(elem)

            self.assertFalse(pcm.for_used()['passed'])
            self.assertTrue(pcm.for_used(comprehension=True)['passed'])

            self.assertFalse(pcm.for_used({"range": (0, 10, 2)})['passed'])
            self.assertTrue(pcm.for_used({"range": (0, 10, 2)}, comprehension=True)['passed'])

            self.assertFalse(pcm.for_used({"range": (10,)})['passed'])
            self.assertFalse(pcm.for_used({"range": (15,)})['passed'])
            self.assertFalse(pcm.for_used({"range": (0, 10)})['passed'])
            self.assertFalse(pcm.for_used({"range": (1, 10)})['passed'])
            self.assertFalse(pcm.for_used({"range": (0, 10, 1)})['passed'])
            self.assertFalse(pcm.for_used({"range": (10,)}, comprehension=True)['passed'])
            self.assertFalse(pcm.for_used({"range": (15,)}, comprehension=True)['passed'])
            self.assertFalse(pcm.for_used({"range": (0, 10)}, comprehension=True)['passed'])
            self.assertFalse(pcm.for_used({"range": (1, 10)}, comprehension=True)['passed'])
            self.assertFalse(pcm.for_used({"range": (0, 10, 1)}, comprehension=True)['passed'])

            self.assertFalse(pcm.for_used({"variable": "abc"})['passed'])
            self.assertFalse(pcm.for_used({"variable": "abc"}, comprehension=True)['passed'])

    def test_for_used_comprehension_iteration(self):
        source_code = [
            textwrap.dedent("""
                test_object = [1, 2, 3, 4]
                [print(elem) for elem in test_object]
            """),
            textwrap.dedent("""
                test_object = [1, 2, 3, 4]
                [print(elem) for elem in test_object.reverse()]
            """),
            textwrap.dedent("""
                test_object = {1: "1", 2: "2", 3: "3", 4: "4"}
                new_dct = {key: value for key, value in test_object.items()}
            """),
            textwrap.dedent("""
                test_object = [1, 2, 3, 4, 5]
                new_set = {x * 2 for x in test_object}
            """)
        ]

        for elem in source_code:
            pcm = PyCheckMate(elem)

            self.assertFalse(pcm.for_used()['passed'])
            self.assertTrue(pcm.for_used(comprehension=True)['passed'])

            self.assertFalse(pcm.for_used({"range": (0, 10)})['passed'])
            self.assertFalse(pcm.for_used({"range": (0, 10, 2)})['passed'])
            self.assertFalse(pcm.for_used({"range": (10,)})['passed'])
            self.assertFalse(pcm.for_used({"range": (15,)})['passed'])
            self.assertFalse(pcm.for_used({"range": (1, 10)})['passed'])
            self.assertFalse(pcm.for_used({"range": (0, 10, 1)})['passed'])
            self.assertFalse(pcm.for_used({"range": (0, 10)}, comprehension=True)['passed'])
            self.assertFalse(pcm.for_used({"range": (0, 10, 2)}, comprehension=True)['passed'])
            self.assertFalse(pcm.for_used({"range": (10,)}, comprehension=True)['passed'])
            self.assertFalse(pcm.for_used({"range": (15,)}, comprehension=True)['passed'])
            self.assertFalse(pcm.for_used({"range": (1, 10)}, comprehension=True)['passed'])
            self.assertFalse(pcm.for_used({"range": (0, 10, 1)}, comprehension=True)['passed'])

            self.assertFalse(pcm.for_used({"variable": "abc"})['passed'])
            self.assertFalse(pcm.for_used({"variable": "abc"}, comprehension=True)['passed'])

            self.assertFalse(pcm.for_used({"variable": "test_object"})['passed'])
            self.assertTrue(pcm.for_used({"variable": "test_object"}, comprehension=True)['passed'])


class TestFunctionUsesFor(unittest.TestCase):

    def test_function_not_uses_for(self):
        source_code = [
        textwrap.dedent("""
            def test():
                a = 2
                b = 3
                c = a + b
        """),
        textwrap.dedent("""
            def test():
                a = 2
                b = 3
                c = a + b
            for x in range(10):
                print(x)
                
            [x**2 for x in range(0, 10, 2)]
        """),
        ]
        for elem in source_code:
            pcm = PyCheckMate(elem)

            self.assertFalse(pcm.function_uses_for("test")['passed'])
            self.assertFalse(pcm.function_uses_for("test", comprehension=True)['passed'])

            self.assertFalse(pcm.function_uses_for("test", {"range": (10,)})['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (0, 10)})['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (0, 10, 1)})['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (10,)}, comprehension=True)['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (0, 10)}, comprehension=True)['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (0, 10, 1)}, comprehension=True)['passed'])


            self.assertFalse(pcm.function_uses_for("test", {"variable": "abc"})['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"variable": "abc"}, comprehension=True)['passed'])




    def test_function_uses_for_range_stop(self):
        source_code = textwrap.dedent("""
            def test():
                for x in range(10):
                    print(x)
        """)
        pcm = PyCheckMate(source_code)

        self.assertTrue(pcm.function_uses_for("test")['passed'])
        self.assertTrue(pcm.function_uses_for("test", comprehension=True)['passed'])

        self.assertTrue(pcm.function_uses_for("test", {"range": (10,)})['passed'])
        self.assertTrue(pcm.function_uses_for("test", {"range": (0, 10)})['passed'])
        self.assertTrue(pcm.function_uses_for("test", {"range": (0, 10, 1)})['passed'])
        self.assertTrue(pcm.function_uses_for("test", {"range": (10,)}, comprehension=True)['passed'])
        self.assertTrue(pcm.function_uses_for("test", {"range": (0, 10)}, comprehension=True)['passed'])
        self.assertTrue(pcm.function_uses_for("test", {"range": (0, 10, 1)}, comprehension=True)['passed'])

        self.assertFalse(pcm.function_uses_for("test", {"range": (15,)})['passed'])
        self.assertFalse(pcm.function_uses_for("test", {"range": (1, 10)})['passed'])
        self.assertFalse(pcm.function_uses_for("test", {"range": (0, 10, 2)})['passed'])
        self.assertFalse(pcm.function_uses_for("test", {"range": (15,)}, comprehension=True)['passed'])
        self.assertFalse(pcm.function_uses_for("test", {"range": (1, 10)}, comprehension=True)['passed'])
        self.assertFalse(pcm.function_uses_for("test", {"range": (0, 10, 2)}, comprehension=True)['passed'])

        self.assertFalse(pcm.function_uses_for("test", {"variable": "abc"})['passed'])
        self.assertFalse(pcm.function_uses_for("test", {"variable": "abc"}, comprehension=True)['passed'])

    def test_function_uses_for_range_start_stop(self):
        source_code = textwrap.dedent("""
            def test():
                for x in range(0, 10):
                    print(x)
        """)
        pcm = PyCheckMate(source_code)

        self.assertTrue(pcm.function_uses_for("test")['passed'])
        self.assertTrue(pcm.function_uses_for("test",comprehension=True)['passed'])

        self.assertTrue(pcm.function_uses_for("test", {"range": (10,)})['passed'])
        self.assertTrue(pcm.function_uses_for("test", {"range": (0, 10)})['passed'])
        self.assertTrue(pcm.function_uses_for("test", {"range": (0, 10, 1)})['passed'])
        self.assertTrue(pcm.function_uses_for("test", {"range": (10,)}, comprehension=True)['passed'])
        self.assertTrue(pcm.function_uses_for("test", {"range": (0, 10)}, comprehension=True)['passed'])
        self.assertTrue(pcm.function_uses_for("test", {"range": (0, 10, 1)}, comprehension=True)['passed'])

        self.assertFalse(pcm.function_uses_for("test", {"range": (15,)})['passed'])
        self.assertFalse(pcm.function_uses_for("test", {"range": (1, 10)})['passed'])
        self.assertFalse(pcm.function_uses_for("test", {"range": (0, 10, 2)})['passed'])
        self.assertFalse(pcm.function_uses_for("test", {"range": (15,)}, comprehension=True)['passed'])
        self.assertFalse(pcm.function_uses_for("test", {"range": (1, 10)}, comprehension=True)['passed'])
        self.assertFalse(pcm.function_uses_for("test", {"range": (0, 10, 2)}, comprehension=True)['passed'])

        self.assertFalse(pcm.function_uses_for("test", {"variable": "abc"})['passed'])
        self.assertFalse(pcm.function_uses_for("test", {"variable": "abc"}, comprehension=True)['passed'])

    def test_function_uses_for_range_start_stop_step(self):
        source_code = textwrap.dedent("""
            def test():
                for x in range(0, 10, 2):
                    print(x)
        """)

        pcm = PyCheckMate(source_code)

        self.assertTrue(pcm.function_uses_for("test")['passed'])
        self.assertTrue(pcm.function_uses_for("test", comprehension=True)['passed'])


        self.assertTrue(pcm.function_uses_for("test", {"range": (0, 10, 2)})['passed'])
        self.assertTrue(pcm.function_uses_for("test", {"range": (0, 10, 2)}, comprehension=True)['passed'])

        self.assertFalse(pcm.function_uses_for("test", {"range": (10,)})['passed'])
        self.assertFalse(pcm.function_uses_for("test", {"range": (15,)})['passed'])
        self.assertFalse(pcm.function_uses_for("test", {"range": (0, 10)})['passed'])
        self.assertFalse(pcm.function_uses_for("test", {"range": (1, 10)})['passed'])
        self.assertFalse(pcm.function_uses_for("test", {"range": (0, 10, 1)})['passed'])
        self.assertFalse(pcm.function_uses_for("test", {"range": (10,)}, comprehension=True)['passed'])
        self.assertFalse(pcm.function_uses_for("test", {"range": (15,)}, comprehension=True)['passed'])
        self.assertFalse(pcm.function_uses_for("test", {"range": (0, 10)}, comprehension=True)['passed'])
        self.assertFalse(pcm.function_uses_for("test", {"range": (1, 10)}, comprehension=True)['passed'])
        self.assertFalse(pcm.function_uses_for("test", {"range": (0, 10, 1)}, comprehension=True)['passed'])

        self.assertFalse(pcm.function_uses_for("test", {"variable": "abc"})['passed'])
        self.assertFalse(pcm.function_uses_for("test", {"variable": "abc"}, comprehension=True)['passed'])

    def test_for_used_iteration(self):
        source_code = [
            textwrap.dedent("""
                def test():
                    test_object = [1, 2, 3, 4]
                    for elem in test_object:
                        print(elem)
            """),
            textwrap.dedent("""
                def test():
                    test_object = [1, 2, 3, 4]
                    for elem in test_object.reverse():
                        print(elem)
            """),
            textwrap.dedent("""
                def test():
                    test_object = {1: "1", 2: "2", 3: "3", 4: "4"}
                    for elem in test_object:
                        print(elem)
            """),
            textwrap.dedent("""
                def test():
                    test_object = {1: "1", 2: "2", 3: "3", 4: "4"}
                    for key, value in test_object.items():
                        print(key, value)
            """),
            textwrap.dedent("""
                def test():
                    test_object = {1: "1", 2: "2", 3: "3", 4: "4"}
                    for elem in test_object.keys():
                        print(elem)
            """),
            textwrap.dedent("""
                def test():
                    test_object = {1: "1", 2: "2", 3: "3", 4: "4"}
                    for elem in test_object.values():
                        print(elem)
            """),
        ]

        for elem in source_code:
            pcm = PyCheckMate(elem)

            self.assertTrue(pcm.function_uses_for("test")['passed'])
            self.assertTrue(pcm.function_uses_for("test", comprehension=True)['passed'])

            self.assertFalse(pcm.function_uses_for("test", {"range": (0, 10)})['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (0, 10, 2)})['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (10,)})['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (15,)})['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (1, 10)})['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (0, 10, 1)})['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (0, 10)}, comprehension=True)['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (0, 10, 2)}, comprehension=True)['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (10,)}, comprehension=True)['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (15,)}, comprehension=True)['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (1, 10)}, comprehension=True)['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (0, 10, 1)}, comprehension=True)['passed'])

            self.assertFalse(pcm.function_uses_for("test", {"variable": "abc"})['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"variable": "abc"}, comprehension=True)['passed'])

            self.assertTrue(pcm.function_uses_for("test", {"variable": "test_object"})['passed'])
            self.assertTrue(pcm.function_uses_for("test", {"variable": "test_object"}, comprehension=True)['passed'])

    def test_function_uses_for_comprehension_range_stop(self):
        source_code = [
            textwrap.dedent("""
                def test():
                    [x**2 for x in range(10)]
            """),
            textwrap.dedent("""
                def test():
                    quadrate = [x**2 for x in range(10)]
            """),
            textwrap.dedent("""
                def test():
                    {x: x**2 for x in range(10)}
            """),
            textwrap.dedent("""
                def test():
                    squared_dict = {x: x**2 for x in range(10)}
            """),
            textwrap.dedent("""
                def test():
                    {x**2 for x in range(10)}
            """),
            textwrap.dedent("""
                def test():
                    unique_squares = {x**2 for x in range(10)}
            """),
        ]

        for elem in source_code:
            pcm = PyCheckMate(elem)

            self.assertFalse(pcm.function_uses_for("test")['passed'])
            self.assertTrue(pcm.function_uses_for("test", comprehension=True)['passed'])

            self.assertFalse(pcm.function_uses_for("test", {"range": (10,)})['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (0, 10)})['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (0, 10, 1)})['passed'])

            self.assertTrue(pcm.function_uses_for("test", {"range": (10,)}, comprehension=True)['passed'])
            self.assertTrue(pcm.function_uses_for("test", {"range": (0, 10)}, comprehension=True)['passed'])
            self.assertTrue(pcm.function_uses_for("test", {"range": (0, 10, 1)}, comprehension=True)['passed'])

            self.assertFalse(pcm.function_uses_for("test", {"range": (15,)})['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (1, 10)})['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (0, 10, 2)})['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (15,)}, comprehension=True)['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (1, 10)}, comprehension=True)['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (0, 10, 2)}, comprehension=True)['passed'])

            self.assertFalse(pcm.function_uses_for("test", {"variable": "abc"})['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"variable": "abc"}, comprehension=True)['passed'])


    def test_function_uses_for_comprehension_range_start_stop(self):
        source_code = [
            textwrap.dedent("""
                def test():
                    [x**2 for x in range(0, 10)]
            """),
            textwrap.dedent("""
                def test():
                    quadrate = [x**2 for x in range(0, 10)]
            """),
            textwrap.dedent("""
                def test():
                    {x: x**2 for x in range(0, 10)}
            """),
            textwrap.dedent("""
                def test():
                    squared_dict = {x: x**2 for x in range(0, 10)}
            """),
            textwrap.dedent("""
                def test():
                    {x**2 for x in range(0, 10)}
            """),
            textwrap.dedent("""
                def test():
                    unique_squares = {x**2 for x in range(0, 10)}
            """),
        ]

        for elem in source_code:
            pcm = PyCheckMate(elem)

            self.assertFalse(pcm.function_uses_for("test")['passed'])
            self.assertTrue(pcm.function_uses_for("test", comprehension=True)['passed'])

            self.assertFalse(pcm.function_uses_for("test", {"range": (10,)})['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (0, 10)})['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (0, 10, 1)})['passed'])
            self.assertTrue(pcm.function_uses_for("test", {"range": (10,)}, comprehension=True)['passed'])
            self.assertTrue(pcm.function_uses_for("test", {"range": (0, 10)}, comprehension=True)['passed'])
            self.assertTrue(pcm.function_uses_for("test", {"range": (0, 10, 1)}, comprehension=True)['passed'])

            self.assertFalse(pcm.function_uses_for("test", {"range": (15,)})['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (1, 10)})['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (0, 10, 2)})['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (15,)}, comprehension=True)['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (1, 10)}, comprehension=True)['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (0, 10, 2)}, comprehension=True)['passed'])

            self.assertFalse(pcm.function_uses_for("test", {"variable": "abc"})['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"variable": "abc"}, comprehension=True)['passed'])

    def test_for_used_comprehension_range_start_stop_step(self):
        source_code = [
            textwrap.dedent("""
                def test():
                    [x**2 for x in range(0, 10, 2)]
            """),
            textwrap.dedent("""
                def test():
                    quadrate = [x**2 for x in range(0, 10, 2)]
            """),
            textwrap.dedent("""
                def test():
                    {x: x**2 for x in range(0, 10, 2)}
            """),
            textwrap.dedent("""
                def test():
                    squared_dict = {x: x**2 for x in range(0, 10, 2)}
            """),
            textwrap.dedent("""
                def test():
                    {x**2 for x in range(0, 10, 2)}
            """),
            textwrap.dedent("""
                def test():
                    unique_squares = {x**2 for x in range(0, 10, 2)}
            """),
        ]

        for elem in source_code:
            pcm = PyCheckMate(elem)

            self.assertFalse(pcm.function_uses_for("test")['passed'])
            self.assertTrue(pcm.function_uses_for("test", comprehension=True)['passed'])

            self.assertFalse(pcm.function_uses_for("test", {"range": (0, 10, 2)})['passed'])
            self.assertTrue(pcm.function_uses_for("test", {"range": (0, 10, 2)}, comprehension=True)['passed'])

            self.assertFalse(pcm.function_uses_for("test", {"range": (10,)})['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (15,)})['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (0, 10)})['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (1, 10)})['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (0, 10, 1)})['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (10,)}, comprehension=True)['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (15,)}, comprehension=True)['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (0, 10)}, comprehension=True)['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (1, 10)}, comprehension=True)['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (0, 10, 1)}, comprehension=True)['passed'])

            self.assertFalse(pcm.function_uses_for("test", {"variable": "abc"})['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"variable": "abc"}, comprehension=True)['passed'])

    def test_function_uses_for_comprehension_iteration(self):
        source_code = [
            textwrap.dedent("""
                def test():
                    test_object = [1, 2, 3, 4]
                    [print(elem) for elem in test_object]
            """),
            textwrap.dedent("""
                def test():
                    test_object = [1, 2, 3, 4]
                    [print(elem) for elem in test_object.reverse()]
            """),
            textwrap.dedent("""
                def test():
                    test_object = {1: "1", 2: "2", 3: "3", 4: "4"}
                    new_dct = {key: value for key, value in test_object.items()}
            """),
            textwrap.dedent("""
                def test():
                    test_object = [1, 2, 3, 4, 5]
                    new_set = {x * 2 for x in test_object}
            """)
        ]

        for elem in source_code:
            pcm = PyCheckMate(elem)

            self.assertFalse(pcm.function_uses_for("test")['passed'])
            self.assertTrue(pcm.function_uses_for("test", comprehension=True)['passed'])

            self.assertFalse(pcm.function_uses_for("test", {"range": (0, 10)})['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (0, 10, 2)})['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (10,)})['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (15,)})['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (1, 10)})['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (0, 10, 1)})['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (0, 10)}, comprehension=True)['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (0, 10, 2)}, comprehension=True)['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (10,)}, comprehension=True)['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (15,)}, comprehension=True)['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (1, 10)}, comprehension=True)['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"range": (0, 10, 1)}, comprehension=True)['passed'])

            self.assertFalse(pcm.function_uses_for("test", {"variable": "abc"})['passed'])
            self.assertFalse(pcm.function_uses_for("test", {"variable": "abc"}, comprehension=True)['passed'])

            self.assertFalse(pcm.function_uses_for("test", {"variable": "test_object"})['passed'])
            self.assertTrue(pcm.function_uses_for("test", {"variable": "test_object"}, comprehension=True)['passed'])