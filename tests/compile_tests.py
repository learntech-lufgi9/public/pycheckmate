import unittest
import textwrap
from pycheckmate import PyCheckMate


class TestDoesCompile(unittest.TestCase):

    def test_does_not_compile_synatxerror_duplicated_argument(self):
        source_code = textwrap.dedent("""
        def sum_orders(*bill, **bill):
            result = 0
            for i in *bill:
                result += i
            return result
        """)

        pcm = PyCheckMate(source_code)

        self.assertFalse(pcm.does_compile()["passed"])

    def test_does_not_compile_synatxerror_return_outside_function(self):
        source_code = [
            textwrap.dedent("""
                def invert (image):
                    inverted_image=[]
                    
                for pixel in row:
                    inverted_pixel=255-pixel
                    inverted_row.append(inverted_pixel)
                inverted_image.append(inverted_row)
                
                return inverted_image
            """),
            textwrap.dedent("""
                add_three(5, 10, 15)
                return 5 + 10 + 15
                result = add_three(5, 10, 15)
            """),
            textwrap.dedent("""
            def sum_all_orders(*bills):
                concatedBills = []
                for i in bills:
                    concatedBills.extend(i)
            return sum(concatedBills)
            """)
        ]

        for elem in source_code:
            pcm = PyCheckMate(elem)

            self.assertFalse(pcm.does_compile()["passed"])

    def test_does_not_compile_synatxerror_cant_use_starred_expression_here(self):
        source_code = [
            textwrap.dedent("""
                def sum_all_orders(*args):
                    bill_value = 0
                    for elem in *args:
                        bill_value += sum(elem)
                    return bill_value        
            """),
            textwrap.dedent("""
            def average_price_petrol(*args):
                length=0
                for i in *args:
                    length+=1
                return sum(*args)/length
            
            """)
        ]

        for elem in source_code:
            pcm = PyCheckMate(elem)

            self.assertFalse(pcm.does_compile()["passed"])

    def test_does_not_compile_identationerror_expected_an_indented_block(self):
        source_code = [
            textwrap.dedent("""
                def fehlerhafte_funktion():
                print("Dieser Code hat einen IndentationError.")
                print("Die Einrückung ist nicht korrekt.")
                
                # Funktionsaufruf
                fehlerhafte_funktion()    
            """),
            textwrap.dedent("""
                def average_price_petrol(*args):
                    length=0
                        for i in *args:
                        length+=1
                    return sum(*args)/length

            """),
            textwrap.dedent("""
                 temperatures_celsius = [8.4, 15.0, 12.5, 8.4, -1.9, 15.0, 2.7]
                #del temperatures_celsius[3]
                temperatures_celsius.pop(3)
                
                 def test():
                    a = 1
                    b = 2            
            """)
        ]

        for elem in source_code:
            pcm = PyCheckMate(elem)

            self.assertFalse(pcm.does_compile()["passed"])