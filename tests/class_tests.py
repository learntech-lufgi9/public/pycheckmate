import unittest
import textwrap
from pycheckmate import PyCheckMate


class TestHasClass(unittest.TestCase):

    def test_has_not_class(self):
        source_code = textwrap.dedent("""
            class Vehicle:
                def __init__(self, coloured, construction_year, mileage):
                    self.coloured = coloured
                    self.construction_year = construction_year
                    self.mileage = mileage
            
                def __str__(self):
                    return "The vehicle is {}, was manufactured in the year {} and has run {} kilometres so far.".format(self.colour, self.construction_year, self.mileage)
            
                def driving(self, km):
                    if(km > 0):
                        self.mileage += km
        """)
        pcm = PyCheckMate(source_code)

        self.assertFalse(pcm.has_class("TestClass")['passed'])
        self.assertFalse(pcm.has_class("TestClass", 0)['passed'])
        self.assertFalse(pcm.has_class("TestClass", 1)['passed'])

    def test_has_class(self):
        source_code = textwrap.dedent("""
            class Vehicle:
                def __init__(self):
                    self.coloured = "red"
                    self.construction_year = 1994
                    self.mileage = 225000

                def __str__(self):
                    return "The vehicle is {}, was manufactured in the year {} and has run {} kilometres so far.".format(self.colour, self.construction_year, self.mileage)

                def driving(self, km):
                    if(km > 0):
                        self.mileage += km
        """)
        pcm = PyCheckMate(source_code)

        self.assertTrue(pcm.has_class("Vehicle")['passed'])
        self.assertFalse(pcm.has_class("Vehicle", 0)['passed'])
        self.assertTrue(pcm.has_class("Vehicle", 1)['passed'])

    def test_has_class_params(self):
        source_code = [
        textwrap.dedent("""
            class Vehicle:
                def __init__(self, coloured, construction_year, mileage):
                    self.coloured = coloured
                    self.construction_year = construction_year
                    self.mileage = mileage
            
                def __str__(self):
                    return "The vehicle is {}, was manufactured in the year {} and has run {} kilometres so far.".format(self.colour, self.construction_year, self.mileage)
            
                def driving(self, km):
                    if(km > 0):
                        self.mileage += km
        """),
        textwrap.dedent("""
            class Vehicle:
                def __init__(self, coloured: str, construction_year = 2000, mileage: int = 0):
                    self.coloured = coloured
                    self.construction_year = construction_year
                    self.mileage = mileage

                def __str__(self):
                    return "The vehicle is {}, was manufactured in the year {} and has run {} kilometres so far.".format(self.colour, self.construction_year, self.mileage)

                def driving(self, km):
                    if(km > 0):
                        self.mileage += km
        """)
        ]

        for elem in source_code:
            pcm = PyCheckMate(elem)

            self.assertTrue(pcm.has_class("Vehicle")['passed'])
            self.assertFalse(pcm.has_class("Vehicle", 0)['passed'])
            self.assertFalse(pcm.has_class("Vehicle", 1)['passed'])
            self.assertTrue(pcm.has_class("Vehicle", 4)['passed'])

class TestClassHasParameters(unittest.TestCase):

    def test_class_has_not_parameters(self):
        source_code = textwrap.dedent("""
            class Vehicle:
                def __init__(self):
                    self.coloured = "red"
                    self.construction_year = 1994
                    self.mileage = 225000
            
                def __str__(self):
                    return "The vehicle is {}, was manufactured in the year {} and has run {} kilometres so far.".format(self.colour, self.construction_year, self.mileage)
            
                def driving(self, km):
                    if(km > 0):
                        self.mileage += km
        """)

        pcm = PyCheckMate(source_code)

        self.assertFalse(pcm.class_has_parameters("TestClass", {"coloured": {}, "construction_year": {}, "mileage": {}})['passed'])
        self.assertFalse(pcm.class_has_parameters("Vehicle", {"coloured": {}, "construction_year": {}, "mileage": {}})['passed'])
        self.assertFalse(pcm.class_has_parameters("Vehicle", {}, True)['passed'])
        self.assertFalse(pcm.class_has_parameters("Vehicle", {}, False, True)['passed'])
        self.assertFalse(pcm.class_has_parameters("Vehicle", {}, True, True)['passed'])

        self.assertFalse(pcm.class_has_parameters("Vehicle", {}, "args")['passed'])
        self.assertFalse(pcm.class_has_parameters("Vehicle", {}, False, "kwargs")['passed'])
        self.assertFalse(pcm.class_has_parameters("Vehicle", {}, "args", "kwargs")['passed'])

    def test_class_has_parameters_args(self):
        source_code = textwrap.dedent("""
            class Vehicle:
                def __init__(self, coloured: str, construction_year = 2000, mileage: int = 0):
                    self.coloured = coloured
                    self.construction_year = construction_year
                    self.mileage = mileage

                def __str__(self):
                    return "The vehicle is {}, was manufactured in the year {} and has run {} kilometres so far.".format(self.colour, self.construction_year, self.mileage)

                def driving(self, km):
                    if(km > 0):
                        self.mileage += km
        """)


        pcm = PyCheckMate(source_code)

        self.assertTrue(pcm.class_has_parameters("Vehicle", {"coloured": {}, "construction_year": {}, "mileage": {}})['passed'])
        self.assertTrue(pcm.class_has_parameters("Vehicle", {"coloured": {"type": str}, "construction_year": {'default': 2000}, "mileage": {"type": int, 'default': 0}})['passed'])

        self.assertFalse(pcm.class_has_parameters("Vehicle", {"coloured": {"type": str}, "construction_year": {'default': 0}, "mileage": {"type": int, 'default': 0}})['passed'])
        self.assertFalse(pcm.class_has_parameters("Vehicle", {"coloured": {"type": int}, "construction_year": {'default': 2000}, "mileage": {"type": int, 'default': 0}})['passed'])

        self.assertFalse(pcm.class_has_parameters("Vehicle", {}, True)['passed'])
        self.assertFalse(pcm.class_has_parameters("Vehicle", {}, False, True)['passed'])
        self.assertFalse(pcm.class_has_parameters("Vehicle", {}, True, True)['passed'])
        self.assertFalse(pcm.class_has_parameters("Vehicle", {}, "args")['passed'])
        self.assertFalse(pcm.class_has_parameters("Vehicle", {}, False, "kwargs")['passed'])
        self.assertFalse(pcm.class_has_parameters("Vehicle", {}, "args", "kwargs")['passed'])

    def test_class_has_parameters_vararg(self):
        source_code = textwrap.dedent("""
            class Vehicle:
                def __init__(self, *args):
                    self.coloured = "red"
                    self.construction_year = 2000
                    self.mileage = 0

                def __str__(self):
                    return "The vehicle is {}, was manufactured in the year {} and has run {} kilometres so far.".format(self.colour, self.construction_year, self.mileage)

                def driving(self, km):
                    if(km > 0):
                        self.mileage += km
        """)

        pcm = PyCheckMate(source_code)

        self.assertTrue(pcm.class_has_parameters("Vehicle", {}, True)['passed'])
        self.assertTrue(pcm.class_has_parameters("Vehicle", {}, "args")['passed'])

        self.assertFalse(pcm.class_has_parameters("Vehicle", {}, "test_args")['passed'])
        self.assertFalse(pcm.class_has_parameters("Vehicle", {}, True, True)['passed'])
        self.assertFalse(pcm.class_has_parameters("Vehicle", {}, "args", True)['passed'])

    def test_class_has_parameters_kwarg(self):
        source_code = textwrap.dedent("""
            class Vehicle:
                def __init__(self, **kwarg):
                    self.coloured = "red"
                    self.construction_year = 2000
                    self.mileage = 0

                def __str__(self):
                    return "The vehicle is {}, was manufactured in the year {} and has run {} kilometres so far.".format(self.colour, self.construction_year, self.mileage)

                def driving(self, km):
                    if(km > 0):
                        self.mileage += km
        """)

        pcm = PyCheckMate(source_code)

        self.assertTrue(pcm.class_has_parameters("Vehicle", {}, False, True)['passed'])
        self.assertTrue(pcm.class_has_parameters("Vehicle", {}, False, "kwarg")['passed'])

        self.assertFalse(pcm.class_has_parameters("Vehicle", {}, False, "test_kwarg")['passed'])
        self.assertFalse(pcm.class_has_parameters("Vehicle", {}, True, True)['passed'])
        self.assertFalse(pcm.class_has_parameters("Vehicle", {}, True, "kwarg")['passed'])


    def test_class_has_parameters_multiple_classes(self):
        source_code = textwrap.dedent("""
            class Vehicle:
                def __init__(self, colour, construction_year, mileage):
                    self.colour = colour
                    self.construction_year = construction_year
                    self.mileage = mileage
                def drive(self, km):
                    self.mileage += km
                def __str__(self):
                    return f"The Vehicle is {self.colour}, was manufactured in the year {self.construction_year} and has run {self.mileage} kilometres so far."
                
            class Truck(Vehicle):
                def __init__(self, num_trailers = 0, *args, **kwargs):
                    super().__init__(*args, **kwargs)
                    self.num_trailers = num_trailers
        """)

        pcm = PyCheckMate(source_code)

        self.assertTrue(pcm.class_has_parameters("Truck", {'num_trailers': {'default': 0}})['passed'])



class TestClassHasAttributes(unittest.TestCase):

    def test_class_has_attributes(self):
        source_code = textwrap.dedent("""
            class Vehicle:
                def __init__(self, coloured: str, construction_year = 2000, mileage: int = 0):
                    self.coloured = coloured
                    self.construction_year = construction_year
                    self.mileage = mileage

                def __str__(self):
                    return "The vehicle is {}, was manufactured in the year {} and has run {} kilometres so far.".format(self.colour, self.construction_year, self.mileage)

                def driving(self, km):
                    if(km > 0):
                        self.mileage += km
        """)

        pcm = PyCheckMate(source_code)

        self.assertTrue(pcm.class_has_attributes("Vehicle", {"coloured", "construction_year", "mileage"}, )['passed'])
        self.assertTrue(pcm.class_has_attributes("Vehicle", {"coloured", "construction_year"}, )['passed'])
        self.assertTrue(pcm.class_has_parameters("Vehicle", {},)['passed'])

        self.assertFalse(pcm.class_has_attributes("Vehicle", {"test_attribute"}, )['passed'])
        self.assertFalse(pcm.class_has_attributes("Vehicle", {"test_attribute", "construction_year", "mileage"}, )['passed'])
        self.assertFalse(pcm.class_has_attributes("Vehicle", {"test_attribute", "coloured", "construction_year", "mileage"}, )['passed'])

    def test_class_has_attributes_3(self):
        source_code = [
        textwrap.dedent("""
            class MyRange:
    
                def __init__(self, start, stop):
                    self.start = start
                    self.stop = stop
                    self.current = start
            
                def __iter__(self):
                    return self
            
                def __next__(self):
                    if self.current < self.stop:
                        value = self.current
                        self.current += 1
                        return value
                    else:
                        raise StopIteration
        """),
        textwrap.dedent("""
              class MyRange:

                  def __init__(self, start, stop):
                      self.start = start
                      self.stop = stop
                      self.current = start

                  def __iter__(self):
                      return self

                  def __next__(self):
                      if self.current < self.stop:
                          value = self.current
                          self.current += 1
                          return value
                      else:
                          raise StopIteration
          """)]

        for elem in source_code:
            pcm = PyCheckMate(elem)

            self.assertTrue(pcm.class_has_attributes("MyRange", {"start", "stop"}, )['passed'])

    def test_class_has_attributes(self):
        source_code = textwrap.dedent("""
            class PixelRow (Pixel):

                def __init__(self, pixels,value=0):
                    super().__init__(value)
                    self.pixels =[Pixel(x) for x in pixels]
            
                def get_pixel(self,y):
                    lst=[a.get_pixel() for a in self.pixels]
                    return lst[y]
            
                def get_pixel_row(self):
                    return [a.get_pixel() for a in self.pixels]
            
                def set_pixel(self,x, val):
                    self.pixels[x] = Pixel(val)
                    return self.pixels
            
                def __str__(self):
                    return f"Pixel Row:{[a.get_pixel() for a in self.pixels]}"
                
    
        """)

        pcm = PyCheckMate(source_code)

        self.assertTrue(pcm.class_has_attributes("PixelRow", {'pixels'})['passed'])

class TestClassHasFunction(unittest.TestCase):

    def test_class_has_function(self):
        source_code = textwrap.dedent("""
            class Vehicle:
                def __init__(self, coloured: str, construction_year = 2000, mileage: int = 0):
                    self.coloured = coloured
                    self.construction_year = construction_year
                    self.mileage = mileage

                def __str__(self):
                    return "The vehicle is {}, was manufactured in the year {} and has run {} kilometres so far.".format(self.colour, self.construction_year, self.mileage)

                def driving(self, km):
                    if(km > 0):
                        self.mileage += km
        """)
        pcm = PyCheckMate(source_code)

        self.assertTrue(pcm.class_has_function("Vehicle", "driving")['passed'])

        self.assertFalse(pcm.class_has_function("TestClass", "driving")['passed'])
        self.assertFalse(pcm.class_has_function("Vehicle", "test_function")['passed'])