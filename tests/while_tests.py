import unittest
import textwrap
from pycheckmate import PyCheckMate


class TestWhileUsed(unittest.TestCase):

    def test_while_not_used(self):
        source_code = textwrap.dedent("""
            a = 2
            b = 3
            c = a + b
        """)
        pcm = PyCheckMate(source_code)

        self.assertFalse(pcm.while_used()['passed'])

    def test_while_used(self):
        source_code = [
        textwrap.dedent("""
            while(i < 5):
                i += 1
                print(i)
        """),
        textwrap.dedent("""
            def test():
                while(i < 5):
                    i += 1
                    print(i)
        """)
        ]

        for elem in source_code:
            pcm = PyCheckMate(elem)

            self.assertTrue(pcm.while_used()['passed'])

class TestFunctionUsesWhile(unittest.TestCase):

    def test_function_uses_not_while(self):
        source_code = [
        textwrap.dedent("""
            a = 2
            b = 3
            c = a + b
        """),
        textwrap.dedent("""
            def test():
                a = 2
                b = 3
            c = a + b
        """),
        textwrap.dedent("""
            while(i < 5):
                i += 1
                print(i)
        """),
        textwrap.dedent("""
            def test_not_existing():
                while(i < 5):
                    i += 1
                    print(i)
        """),
        ]

        for elem in source_code:
            pcm = PyCheckMate(elem)

            self.assertFalse(pcm.function_uses_while("test")['passed'])

    def test_function_uses_while(self):
        source_code = textwrap.dedent("""
            def test(): 
                while(i < 5):
                    i += 1
                    print(i)
        """)

        pcm = PyCheckMate(source_code)

        self.assertTrue(pcm.function_uses_while("test")['passed'])