import unittest
import textwrap
from pycheckmate import PyCheckMate


class TestTryExceptUsed(unittest.TestCase):

    def test_try_except_not_used(self):
        source_code = [
        textwrap.dedent("""
            a = 2
            b = 3
            c = a + b
        """),
        textwrap.dedent("""
            def test():
                a = 2
                b = 3 
            c = a + b
        """),
        ]

        for elem in source_code:
            pcm = PyCheckMate(elem)

            self.assertFalse(pcm.try_except_used()["passed"])

    def test_try_except_used(self):
        source_code = [
        textwrap.dedent("""
            try:
                a = 2
                b = 3
                c = a + b
            except:
                c = 0
        """),
        textwrap.dedent("""
            try:
                a = 2
                b = 3
                c = a + b
            except NameError:
                c = 0
        """),
        textwrap.dedent("""
            try:
                a = 2
                b = 3
                c = a + b
            except NameError as e:
                c = 0
        """),
        textwrap.dedent("""
            def test():
                try:
                    a = 2
                    b = 3
                    c = a + b
                except:
                    c = 0
        """),
        ]

        for elem in source_code:
            pcm = PyCheckMate(elem)

            self.assertTrue(pcm.try_except_used()["passed"])

class TestFunctionUsesTryExcept(unittest.TestCase):

    def test_function_not_uses_try_except(self):
        source_code = [
        textwrap.dedent("""
            try:
                a = 2
                b = 3
                c = a + b
            except:
                c = 0
        """),
        textwrap.dedent("""
            def test():
                a = 2
                b = 3 
            c = a + b
        """),
        ]

        for elem in source_code:
            pcm = PyCheckMate(elem)

            self.assertFalse(pcm.function_uses_try_except("test")["passed"])

    def test_function_uses_try_except(self):
        source_code = [
        textwrap.dedent("""
            def test():
                try:
                    a = 2
                    b = 3
                    c = a + b
                except:
                    c = 0
        """),
        textwrap.dedent("""
            def test():
                try:
                    a = 2
                    b = 3
                    c = a + b
                except NameError:
                    c = 0
        """),
        textwrap.dedent("""
            def test():
                try:
                    a = 2
                    b = 3
                    c = a + b
                except NameError as e:
                    c = 0
        """)
        ]

        for elem in source_code:
            pcm = PyCheckMate(elem)

            self.assertTrue(pcm.function_uses_try_except("test")["passed"])