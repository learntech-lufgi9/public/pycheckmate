import unittest
import textwrap
from pycheckmate import PyCheckMate


class TestHasVariable(unittest.TestCase):

    def test_has_not_variable(self):
        source_code = textwrap.dedent("""
            a = 2
            b = 3
            c = a + b
        """)
        pcm = PyCheckMate(source_code)

        self.assertFalse(pcm.has_variable("d")['passed'])

    def test_has_variable_single_assign(self):
        source_code = textwrap.dedent("""
            a = 2
            b = 3
            c = a + b
        """)
        pcm = PyCheckMate(source_code)

        self.assertTrue(pcm.has_variable("a")['passed'])
        self.assertTrue(pcm.has_variable("b")['passed'])
        self.assertTrue(pcm.has_variable("c")['passed'])

    def test_has_variable_multiple_assign(self):
        source_code = textwrap.dedent("""
            a = b = 2
            c = a + b
        """)
        pcm = PyCheckMate(source_code)

        self.assertTrue(pcm.has_variable("a")['passed'])
        self.assertTrue(pcm.has_variable("b")['passed'])
        self.assertTrue(pcm.has_variable("c")['passed'])

    def test_has_variable_tuple_assign(self):
        source_code = textwrap.dedent("""  
            a, b = (2, 3)
            c = a + b
        """)
        pcm = PyCheckMate(source_code)

        self.assertTrue(pcm.has_variable("a")['passed'])
        self.assertTrue(pcm.has_variable("b")['passed'])
        self.assertTrue(pcm.has_variable("c")['passed'])


class TestFunctionHasVariable(unittest.TestCase):

    def test_function_has_not_variable(self):
        source_code = textwrap.dedent("""
            def test():
                a = 2
                b = 3
                c = a + b
            d = 1
        
        """)
        pcm = PyCheckMate(source_code)

        self.assertFalse(pcm.function_has_variable("test", "d")['passed'])

    def test_function_has_variable_single_assign(self):
        source_code = textwrap.dedent("""
            def test():
                a = 2
                b = 3
                c = a + b
        """)
        pcm = PyCheckMate(source_code)

        self.assertTrue(pcm.function_has_variable("test", "a")['passed'])
        self.assertTrue(pcm.function_has_variable("test", "b")['passed'])
        self.assertTrue(pcm.function_has_variable("test", "c")['passed'])

    def test_function_has_variable_multiple_assign(self):
        source_code = textwrap.dedent("""
            def test():
                a = b = 2
                c = a + b
        """)
        pcm = PyCheckMate(source_code)

        self.assertTrue(pcm.function_has_variable("test", "a")['passed'])
        self.assertTrue(pcm.function_has_variable("test", "b")['passed'])
        self.assertTrue(pcm.function_has_variable("test", "c")['passed'])

    def test_function_has_variable_tuple_assign(self):
        source_code = textwrap.dedent("""  
            def test():
                a, b = (2, 3)
                c = a + b
        """)
        pcm = PyCheckMate(source_code)

        self.assertTrue(pcm.function_has_variable("test", "a")['passed'])
        self.assertTrue(pcm.function_has_variable("test", "b")['passed'])
        self.assertTrue(pcm.function_has_variable("test", "c")['passed'])