import unittest
import textwrap
from pycheckmate import PyCheckMate


class TestListComprehensionUsed(unittest.TestCase):

    def test_list_comprehension_not_used(self):
        source_code = [
            textwrap.dedent("""
                a = 2
                test_compr = a
            """),
            textwrap.dedent("""
                def test():
                    a = 2
                    test_compr = a
            """),
            textwrap.dedent("""
                {x: x**2 for x in range(10)}
            """),
            textwrap.dedent("""
                test_compr = {x: x**2 for x in range(10)}
            """),
            textwrap.dedent("""
                {x**2 for x in range(10)}
            """),
            textwrap.dedent("""
                test_compr = {x**2 for x in range(10)}
            """),
        ]

        for elem in source_code:
            pcm = PyCheckMate(elem)

            self.assertFalse(pcm.list_comprehension_used()["passed"])
            self.assertFalse(pcm.list_comprehension_used("test_compr")["passed"])

    def test_list_comprehension_used(self):
        source_code = [
            textwrap.dedent("""
                [x**2 for x in range(10)]
            """),
            textwrap.dedent("""
                quadrate = [x**2 for x in range(10)]
            """),
        ]

        for elem in source_code:
            pcm = PyCheckMate(elem)

            self.assertTrue(pcm.list_comprehension_used()["passed"])

    def test_list_comprehension_used_object(self):
        source_code = textwrap.dedent("""
            test_compr = [x**2 for x in range(10)]
        """),

        for elem in source_code:
            pcm = PyCheckMate(elem)

            self.assertTrue(pcm.list_comprehension_used()["passed"])
            self.assertTrue(pcm.list_comprehension_used("test_compr")["passed"])
            self.assertFalse(pcm.list_comprehension_used("test_compr_not_existing")["passed"])


class TestFunctionUsesListComprehension(unittest.TestCase):

    def test_function_uses_not_list_comprehension(self):
        source_code = [
            textwrap.dedent("""
                a = 2
                test_compr = [x**2 for x in range(10)]
            """),
            textwrap.dedent("""
                [x**2 for x in range(10)]
                
                def test():
                    a = 2
                    test_compr = a
            """),
            textwrap.dedent("""
                def test():
                    {x: x**2 for x in range(10)}
            """),
            textwrap.dedent("""
                def test():
                    test_compr = {x: x**2 for x in range(10)}
            """),
            textwrap.dedent("""
                def test():
                    {x**2 for x in range(10)}
            """),
            textwrap.dedent("""
                def test():
                    test_compr = {x**2 for x in range(10)}
            """),
        ]

        for elem in source_code:
            pcm = PyCheckMate(elem)

            self.assertFalse(pcm.function_uses_list_comprehension("test")["passed"])

    def test_function_uses_list_comprehension(self):
        source_code = [
            textwrap.dedent("""
                def test():
                    [x**2 for x in range(10)]
            """),
            textwrap.dedent("""
                def test():
                    quadrate = [x**2 for x in range(10)]
            """),
        ]

        for elem in source_code:
            pcm = PyCheckMate(elem)

            self.assertTrue(pcm.function_uses_list_comprehension("test")["passed"])



