import unittest
import textwrap
from pycheckmate import PyCheckMate


class TestFunctionIsRecursive(unittest.TestCase):

    def test_function_is_not_recursive(self):
        source_code = [
        textwrap.dedent("""
            a = 2
            b = 3
            c = a + b
        """),
        textwrap.dedent("""
            def test():
                a = 2
                b = 3
                c = a + b
        """),
        ]

        for elem in source_code:
            pcm = PyCheckMate(elem)

            self.assertFalse(pcm.function_is_recursive("test")['passed'])

    def test_function_is_recursive_direct(self):
        source_code = [
        textwrap.dedent("""
            def test(n):
                if n == 0:
                    return 1
                else:
                    return n * test(n - 1)
        """),
        textwrap.dedent("""
            def test():
                if n <= 1:
                    return n
                else:
                    return test(n - 1) + test(n - 2)
        """),
        ]

        for elem in source_code:
            pcm = PyCheckMate(elem)

            self.assertTrue(pcm.function_is_recursive("test")['passed'])

    def test_function_is_recursive_indirect(self):
        source_code = [
        textwrap.dedent("""
            def test():
                if n == 0:
                    return True
                else:
                    test_2(n - 1)
                
            def test_2(n):
                if n == 0:
                    return False
                else:
                    return test(n - 1)
        """),
        textwrap.dedent("""
            def test():
                if n <= 1:
                    return n
                else:
                    return test(n - 1) + test(n - 2)
        """),
        ]

        for elem in source_code:
            pcm = PyCheckMate(elem)

            self.assertTrue(pcm.function_is_recursive("test")['passed'])
