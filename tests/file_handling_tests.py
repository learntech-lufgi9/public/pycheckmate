import unittest
import textwrap
from pycheckmate import PyCheckMate


class TestFunctionOpensFile(unittest.TestCase):

    def test_function_not_opens_file(self):
        source_code = [
            textwrap.dedent("""
                f = open("demofile.txt", "r")
                print(f.read())
                f.close()
                
                def test():
                    return 2
            """),
            textwrap.dedent("""
                def test():
                    return 2
            """),
            textwrap.dedent("""
                f = open("demofile.txt", "r")
                print(f.read())
                f.close()
            """)
        ]

        for elem in source_code:
            pcm = PyCheckMate(elem)

            self.assertFalse(pcm.function_opens_file("test")["passed"])

    def test_function_opens_file(self):
        source_code = textwrap.dedent("""
            def test():
                f = open("demofile.txt", "r")
                print(f.read())
                f.close()
                return 2
        """)

        pcm = PyCheckMate(source_code)

        self.assertTrue(pcm.function_opens_file("test")["passed"])

    def test_function_opens_specific_file(self):
        source_code = textwrap.dedent("""
            def test():
                f = open("demofile.txt", "r")
                print(f.read())
                f.close()
                return 2
        """)

        pcm = PyCheckMate(source_code)

        self.assertTrue(pcm.function_opens_file("test", "demofile.txt")["passed"])
        self.assertFalse(pcm.function_opens_file("test", "demofile_not_existing.txt")["passed"])

    def test_function_opens_specific_file_mode(self):
        source_code = textwrap.dedent("""
            def test():
                f = open("demofile.txt", "r")
                print(f.read())
                f.close()
                return 2
        """)

        pcm = PyCheckMate(source_code)

        self.assertTrue(pcm.function_opens_file("test", "demofile.txt", "r")["passed"])
        self.assertFalse(pcm.function_opens_file("test", "demofile_not_existing.txt", "r")["passed"])
        self.assertFalse(pcm.function_opens_file("test", "demofile.txt", "w")["passed"])


class TestFunctionClosesFile(unittest.TestCase):

    def test_function_not_closes_file(self):
        source_code = [
            textwrap.dedent("""
                f = open("demofile.txt", "r")
                print(f.read())
                f.close()

                def test():
                    return 2
            """),
            textwrap.dedent("""
                def test():
                    return 2
            """),
            textwrap.dedent("""
                f = open("demofile.txt", "r")
                print(f.read())
                f.close()
            """),
            textwrap.dedent("""
                def test():
                    f = open("demofile.txt", "r")
                    print(f.read())
            """)
        ]

        for elem in source_code:
            pcm = PyCheckMate(elem)

            self.assertFalse(pcm.function_closes_file("test")["passed"])

    def test_function_closes_file(self):
        source_code = textwrap.dedent("""
            def test():
                f = open("demofile.txt", "r")
                print(f.read())
                f.close()
                return 2
        """)

        pcm = PyCheckMate(source_code)

        self.assertTrue(pcm.function_closes_file("test")["passed"])


class TestFunctionUsesWith(unittest.TestCase):

    def test_function_not_uses_with(self):
        source_code = [
            textwrap.dedent("""
                f = open("demofile.txt", "r")
                print(f.read())
                f.close()

                def test():
                    return 2
            """),
            textwrap.dedent("""
                def test():
                    return 2
            """),
            textwrap.dedent("""
                f = open("demofile.txt", "r")
                print(f.read())
                f.close()
            """),
            textwrap.dedent("""
                def test():
                    f = open("demofile.txt", "r")
                    print(f.read())
            """),
            textwrap.dedent("""
                a = 2
                
                with open('file_path', 'w') as file:
                    file.write('hello world !')
            """)
        ]

        for elem in source_code:
            pcm = PyCheckMate(elem)

            self.assertFalse(pcm.function_uses_with("test")["passed"])

    def test_function_uses_with(self):
        source_code = textwrap.dedent("""
            def test():
                with open('file_path', 'w') as file:
                    file.write('hello world !')
        """)

        pcm = PyCheckMate(source_code)

        self.assertTrue(pcm.function_uses_with("test")["passed"])
