import unittest
import textwrap
from pycheckmate import PyCheckMate


class TestModuleIsImported(unittest.TestCase):

    def test_module_is_not_imported(self):
        source_code = textwrap.dedent("""
            a = 2
            b = 3
            c = a + b
        """)

        pcm = PyCheckMate(source_code)

        self.assertFalse(pcm.module_is_imported("math")["passed"])

    def test_module_is_imported(self):
        source_code = [
        textwrap.dedent("""
            import math
            
            a = 2
            b = 3
            c = a + b
        """),
        textwrap.dedent("""
            from math import isnan

            a = 2
            b = 3
            c = a + b
        """),
        textwrap.dedent("""
            from math import *

            a = 2
            b = 3
            c = a + b
        """),
        ]

        for elem in source_code:
            pcm = PyCheckMate(elem)

            self.assertTrue(pcm.module_is_imported("math")["passed"])


class TestModuleIsImportedFrom(unittest.TestCase):

    def test_module_is_not_imported_from(self):
        source_code = [
        textwrap.dedent("""
            a = 2
            b = 3
            c = a + b
        """),
        textwrap.dedent("""
            import math
            
            a = 2
            b = 3
            c = a + b
        """),
        textwrap.dedent("""
            from math import isinf
            
            a = 2
            b = 3
            c = a + b
        """),
        ]

        for elem in source_code:
            pcm = PyCheckMate(elem)

            self.assertFalse(pcm.module_is_imported_from("isnan", "math")["passed"])

    def test_module_is_imported_from(self):
        source_code = textwrap.dedent("""
            from math import isnan
            
            a = 2
            b = 3
            c = a + b
        """)

        pcm = PyCheckMate(source_code)

        self.assertTrue(pcm.module_is_imported_from("isnan", "math")["passed"])
